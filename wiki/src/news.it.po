# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-08 03:39+0000\n"
"PO-Revision-Date: 2022-07-09 16:08+0000\n"
"Last-Translator: gallium69 <gallium69@riseup.net>\n"
"Language-Team: ita <transitails@inventati.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Content of: <div>
msgid "[[!meta title=\"News\"]]"
msgstr "[[!meta title=\"Notizie\"]]"

#. type: Content of: <p>
#, fuzzy
#| msgid ""
#| "Subscribe to the [[amnesia-news mailing list|about/contact#amnesia-news]] "
#| "to receive the same news by email:"
msgid "Subscribe to our newsletter to receive the same news by email:"
msgstr ""
"Iscriviti alla [[mailing list amnesia-news|about/contact#amnesia-news]] per "
"ricevere tle stesse notizie via email:"

#. type: Content of: <form>
#, fuzzy
#| msgid ""
#| "<input class=\"text\" name=\"email\" value=\"\"/> <input class=\"bold\" "
#| "type=\"submit\" value=\"Subscribe\"/>"
msgid ""
"<input aria-label=\"Email\" name=\"email\" value=\"\"/> <button aria-"
"label=\"Subscribe\" class=\"button\" type=\"submit\">Subscribe</button>"
msgstr ""
"<input class=\"text\" name=\"email\" value=\"\"/> <input class=\"bold\" "
"type=\"submit\" value=\"Subscribe\"/>"

#. type: Content of: <form><p>
msgid "[[Archive of all news|news/archive]]"
msgstr ""

#.  Inlined news 
#.  Feed buttons used by puppet-tails:manifests/website/rss2email.pp
#. [[!inline pages="page(news/*) and !news/archive and !news/*/* and !news/discussion and (currentlang() or news/report_2* or news/test_*) and tagged(announce)"
#. show="10" feeds="yes" feedonly="yes" feedfile="emails" sort="-meta(date) age -path"]]
#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
#| "(currentlang() or news/report_2* or news/test_*)\" show=\"10\" sort=\"-"
#| "meta(date) age -path\"]]"
msgid ""
"[[!inline pages=\"page(news/*) and !news/archive and !news/*/* and !news/"
"discussion and (currentlang() or news/report_2* or news/test_*)\" "
"show=\"10\" sort=\"-meta(date) age -path\"]]"
msgstr ""
"[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
"(currentlang() or news/report_2* or news/test_*)\" show=\"10\" sort=\"-"
"meta(date) age -path\"]]"

#, fuzzy
#~| msgid ""
#~| "[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
#~| "(currentlang() or news/report_2* or news/test_*) and tagged(announce)\" "
#~| "show=\"10\" feeds=\"yes\" feedonly=\"yes\" feedfile=\"emails\" sort=\"-"
#~| "meta(date) age -path\"]]"
#~ msgid ""
#~ "[[!inline pages=\"page(news/*) and !news/archive and !news/*/* and !news/"
#~ "discussion and (currentlang() or news/report_2* or news/test_*) and "
#~ "tagged(announce)\" show=\"10\" feeds=\"yes\" feedonly=\"yes\" "
#~ "feedfile=\"emails\" sort=\"-meta(date) age -path\"]]"
#~ msgstr "<!-- Disabled on purpose on translated version of this page. -->"

#~ msgid ""
#~ "Follow us on Twitter <a href=\"https://twitter.com/"
#~ "tails_live\">@Tails_live</a>."
#~ msgstr ""
#~ "Seguici su Twitter <a href=\"https://twitter.com/"
#~ "tails_live\">@Tails_live</a>."
