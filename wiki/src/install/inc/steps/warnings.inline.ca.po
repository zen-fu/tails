# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-08 22:01+0000\n"
"PO-Revision-Date: 2024-02-23 18:39+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"warnings\">Warnings: Tails is safe but not magic!</h1>\n"
msgstr "<h1 id=\"warnings\">Advertències: Tails és segur però no màgic!</h1>\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/about/warnings\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/about/warnings.ca\" raw=\"yes\" sort=\"age\"]]\n"

#, no-wrap
#~ msgid "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\"]]\n"
#~ msgstr "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\"]]\n"
