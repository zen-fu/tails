# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-08-24 19:55+0000\n"
"PO-Revision-Date: 2024-02-23 18:40+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 5.11 is out\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Mon, 20 Mar 2023 11:36:36 +0000\"]]\n"
msgstr "[[!meta date=\"Mon, 20 Mar 2023 11:36:36 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"features\">New features</h1>\n"
msgstr "<h1 id=\"features\">Nuevas funcionalidades</h1>\n"

#. type: Bullet: '- '
msgid ""
"Tails now uses the [`zram` Linux kernel module](https://en.wikipedia.org/"
"wiki/Zram) to extend the capacity of the computer's memory."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  You can run more applications or use your session for longer periods of time.\n"
"  Tails will handle more load before freezing and become slow more\n"
"  progressively.\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"You can record screencasts using the integrated feature of GNOME. We "
"configured this feature to allow unlimited screencasts. See [[screenshot and "
"screencast|doc/sensitive_documents/screenshot_and_screencast]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Cambios y actualizaciones</h1>\n"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [12.0.4](https://blog.torproject.org/new-release-"
"tor-browser-1204)."
msgstr ""

#. type: Plain text
msgid ""
"- Update *Thunderbird* to [102.9.0](https://www.thunderbird.net/en-US/"
"thunderbird/102.9.0/releasenotes/)."
msgstr ""

#. type: Plain text
msgid "- Redesigned the unlocking section of the Welcome Screen."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img doc/first_steps/welcome_screen/unlock_persistent_storage.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Para más detalles, lee nuestro [[!tails_gitweb debian/changelog desc="
"\"changelog\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<!--\n"
msgstr "<!--\n"

#. type: Plain text
msgid ""
"Copy the known issues from the previous version if they haven't been solved "
"or move them to /support/known_issues if more appropriate."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "-->\n"
msgstr "-->\n"

#. type: Plain text
msgid "None specific to this release."
msgstr "Nada concreto para esta versión."

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.11</h1>\n"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"Para actualizar tu memoria USB de Tails y mantener tu almacenamiento "
"persistente"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.11."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Puedes [[reducir el tamaño de la descarga|doc/upgrade#reduce]] de futuras\n"
"  actualizaciones automáticas haciendo una actualización manual a la última "
"versión.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si no puedes hacer una actualización automática, o si Tails falla al iniciar "
"después de una actualización automática, intenta hacer una [[actualización "
"manual|doc/upgrade#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Para instalar Tails en una nueva memoria USB"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Sigue nuestras instrucciones de instalación:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Instalar desde Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Instalar desde macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Instalar desde Linux|install/linux]]"

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Instalar desde Debian o Ubuntu usando la linea de comandos y GnuPG|install/"
"expert]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>El Almacenamiento Persistente en la memoria USB se "
"perderá si\n"
"instalas en vez de actualizar.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Para sólo descargar"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.11 directly:"
msgstr ""

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Para memorias USB (imagen USB)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Para DVD y máquinas virtuales (imagen ISO)|install/download-iso]]"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"next\">What's coming up?</h1>\n"
msgstr ""

#. type: Plain text
msgid "Tails 5.12 is [[scheduled|contribute/calendar]] for April 18."
msgstr ""
