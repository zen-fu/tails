# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-12-22 09:53+0100\n"
"PO-Revision-Date: 2024-02-23 18:40+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!meta title=\"Tails 5.21\"]]\n"
msgstr "[[!meta title=\"Tails 5.21\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!meta date=\"Fri, 22 Dec 2023 12:00:00 +0000\"]]\n"
msgstr "[[!meta date=\"Fri, 22 Dec 2023 12:00:00 +0000\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Canvis i actualitzacions</h1>\n"

#. type: Bullet: '- '
#, markdown-text
msgid ""
"Help troubleshoot when resizing the system partition fails the first time "
"Tails is started."
msgstr ""
"S'ha millorat l'ajuda per resoldre problemes quan es produeix un error en "
"canviar la mida de la partició del sistema la primera vegada que s'inicia "
"Tails."

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"  [[!img resizing.png link=\"no\" alt=\"Error message: Resizing System "
"Partition Failed - It will be impossible to create a Persistent Storage or "
"apply automatic upgrades.\"]]\n"
msgstr ""
"  [[!img resizing.png link=\"no\" alt=\"Missatge d'error: S'ha produït un "
"error en canviar la mida de la partició del sistema - Serà impossible crear "
"un Emmagatzematge Persistent o aplicar actualitzacions automàtiques.\"]]\n"

#. type: Plain text
#, markdown-text
msgid "- Translate the date displayed in the top navigation bar."
msgstr "- S'ha traduït la data que es mostra a la barra de navegació superior."

#. type: Plain text
#, markdown-text, no-wrap
msgid "  [[!img clock.png link=\"no\" alt=\"Clock showing date in Spanish\"]]\n"
msgstr ""
"  [[!img clock.png link=\"no\" alt=\"El rellotge mostra la data en castellà\""
"]]\n"

#. type: Plain text
#, markdown-text
msgid ""
"- Update *Tor Browser* to "
"[13.0.7](https://blog.torproject.org/new-release-tor-browser-1307)."
msgstr ""
"- S'ha actualitzat el *Navegador Tor* a la versió [13.0.7](https://blog."
"torproject.org/new-release-tor-browser-1307)."

#. type: Plain text
#, markdown-text
msgid "- Update the *Tor* client to 0.4.8.10."
msgstr "- S'ha actualitzat el client de *Tor* a la versió 0.4.8.10."

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Problemes solucionats</h1>\n"

#. type: Plain text
#, markdown-text
msgid ""
"- Fix *Tor Browser* crashing when clicking on the *UBlock* "
"icon. ([[!tails_ticket 20061]])"
msgstr ""
"- S'ha solucionat el bloqueig del *Navegador Tor* quan es fa clic a la icona "
"d'*UBlock*. ([[!tails_ticket 20061]])"

#. type: Plain text
#, markdown-text
msgid "- Make time synchronization more reliable. ([[!tails_ticket 19923]])"
msgstr ""
"- S'ha fet que la sincronització horària sigui més fiable. ([[!tails_ticket "
"19923]])"

#. type: Plain text
#, markdown-text
msgid ""
"- [[!tails_gitlab @BenWestgate]] fixed several issues in the backup feature "
"of *Tails Cloner*:"
msgstr ""
"- En [[!tails_gitlab @BenWestgate]] ha solucionat diversos problemes de la "
"funcionalitat de còpia de seguretat del *Clonador de Tails*:"

#. type: Bullet: '  * '
#, markdown-text
msgid ""
"Remove message about doing a backup when no USB stick is plugged "
"in. ([[!tails_ticket 20063]])"
msgstr ""
"S'ha eliminat el missatge sobre fer una còpia de seguretat quan no hi ha cap "
"llapis USB connectat. ([[!tails_ticket 20063]])"

#. type: Bullet: '  * '
#, markdown-text
msgid ""
"Update available options when plugging in another USB "
"stick. ([[!tails_ticket 20042]])"
msgstr ""
"S'han actualitzat les opcions disponibles en connectar un altre llapis USB. "
"([[!tails_ticket 20042]])"

#. type: Bullet: '  * '
#, markdown-text
msgid ""
"Point to backup instructions when choosing to clone the Persistent Storage.  "
"([[!tails_mr 1305]])"
msgstr ""
"Es mostren les instruccions de còpia de seguretat quan trieu clonar "
"l'Emmagatzematge Persistent. ([[!tails_mr 1305]])"

#. type: Plain text
#, markdown-text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."
msgstr ""
"Per a més detalls, llegiu el nostre [[!tails_gitweb debian/changelog desc="
"\"llistat de canvis\"]]."

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.21</h1>\n"
msgstr "<h1 id=\"get\">Obtenir Tails 5.21</h1>\n"

#. type: Title ##
#, markdown-text, no-wrap
msgid "To upgrade your Tails USB stick and keep your Persistent Storage"
msgstr ""
"Per actualitzar el vostre llapis USB de Tails i mantenir el vostre "
"Emmagatzematge Persistent"

#. type: Plain text
#, markdown-text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.21."
msgstr ""
"- Les actualitzacions automàtiques estan disponibles des de Tails 5.0 o "
"posterior fins a la versió 5.21."

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Podeu [[reduir la mida de la baixada|doc/upgrade#reduce]] de futures\n"
"  actualitzacions automàtiques fent una actualització manual a la darrera "
"versió.\n"

#. type: Bullet: '- '
#, markdown-text
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual "
"upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si no podeu fer una actualització automàtica o si Tails no s'inicia després "
"d'una actualització automàtica, proveu de fer una [[actualització manual|doc/"
"upgrade/#manual]]."

#. type: Title ##
#, markdown-text, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Per instal·lar Tails en un nou llapis USB"

#. type: Plain text
#, markdown-text
msgid "Follow our installation instructions:"
msgstr "Seguiu les nostres instruccions d'instal·lació:"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Instal·lar des de Windows|install/windows]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Instal·lar des de macOS|install/mac]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Instal·lar des de Linux|install/linux]]"

#. type: Bullet: '  - '
#, markdown-text
msgid ""
"[[Install from Debian or Ubuntu using the command line and "
"GnuPG|install/expert]]"
msgstr ""
"[[Instal·lar des de Debian o Ubuntu mitjançant la línia d'ordres i GnuPG|"
"install/expert]]"

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be "
"lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>L'Emmagatzematge Persistent del llapis USB es "
"perdrà si\n"
"instal·leu en comptes d'actualitzar.</p></div>\n"

#. type: Title ##
#, markdown-text, no-wrap
msgid "To download only"
msgstr "Per només baixar"

#. type: Plain text
#, markdown-text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.21 directly:"
msgstr ""
"Si no necessiteu instruccions d'instal·lació o actualització, podeu baixar "
"Tails 5.21 directament:"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Per a llapis USB (imatge USB)|install/download]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Per a DVD i màquines virtuals (imatge ISO)|install/download-iso]]"
