# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2023-07-29 21:22+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Resetting a USB stick using Linux\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/reset.intro\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/reset.intro.tr\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"disks\"></a>\n"
msgstr "<a id=\"disks\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Using the *Disks* utility"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid "<p><b>You might overwrite any hard disk on the computer.</b></p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>If at some point you are not sure about which device to choose, stop proceeding.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Bullet: '1. '
msgid "Make sure that the USB stick that you want to reset is unplugged."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Choose **Applications**&nbsp;▸ **Utilities**&nbsp;▸ **Disks** to start the "
"*Disks* utility."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   A list of all the storage devices on the computer appears in the left pane\n"
"   of the window.\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Plug in the USB stick that you want to reset."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   A new device appears in the list of storage devices. This new device\n"
"   corresponds to the USB stick that you plugged in. Click on it.\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"In the right pane of the window, verify that the device corresponds to the "
"USB stick that you want to reset, its brand, its size, etc."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"To reset the USB stick, click on the [[!img lib/view-more.png alt=\"Drive "
"Options\" class=\"symbolic\" link=\"no\"]] button in the title bar and "
"choose **Format Disk** to erase all the existing partitions on the device."
msgstr ""

#. type: Bullet: '1. '
msgid "In the **Format Disk** dialog:"
msgstr ""

#. type: Bullet: '   - '
msgid ""
"If you want to overwrite all data on the device, choose **Overwrite existing "
"data with zeroes** in the **Erase** menu."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "     <div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"     <p>Overwriting existing data does not erase all data on flash\n"
"     memories, such as USB sticks and SSDs (Solid-State Drives).</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "     <p>See the [[limitations of file deletion|doc/encryption_and_privacy/secure_deletion#spare]].</p>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "     </div>\n"
msgstr "</div>\n"

#. type: Bullet: '   - '
msgid ""
"Choose **Compatible with all systems and devices (MBR/DOS)** in the "
"**Partitioning** menu."
msgstr ""

#. type: Bullet: '1. '
msgid "Click **Format**."
msgstr ""

#. type: Bullet: '1. '
msgid "In the confirmation dialog, click **Format** to confirm."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"To make sure that all the changes are written to the USB stick, click on the"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img lib/media-eject.png alt=\"Eject\" class=\"symbolic\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   button in the title bar.\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Resetting a Tails USB stick from itself"
msgstr ""

#. type: Plain text
msgid ""
"If Tails is your only Linux system, you can generally reset a Tails USB "
"stick directly from that USB stick while running Tails."
msgstr ""

#. type: Bullet: '1.  '
msgid "When starting Tails, add the `toram` boot option in the *Boot Loader*."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    See [[using the *Boot Loader*|advanced_topics/boot_options]].\n"
msgstr ""

#. type: Bullet: '2.  '
msgid ""
"If Tails starts as usual, follow the instructions for [[resetting a USB "
"stick using the *Disks* utility|linux#disks]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    **If the system fails to start**, that means that the computer does not have\n"
"    enough memory for this operation mode. Try with another computer, or find\n"
"    another Linux system, such as another Tails USB stick, to do the reset from.\n"
msgstr ""
