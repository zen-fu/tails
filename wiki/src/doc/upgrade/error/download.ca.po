# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2021-04-05 08:20+0000\n"
"PO-Revision-Date: 2023-11-03 15:12+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Error while downloading the upgrade\"]]\n"
msgstr "[[!meta title=\"Error en baixar l'actualització\"]]\n"

#. type: Plain text
msgid "The upgrade could not be downloaded."
msgstr "No s'ha pogut baixar l'actualització."

#. type: Plain text
msgid ""
"Check your network connection, and restart Tails to try upgrading again."
msgstr ""
"Comproveu la vostra connexió de xarxa i reinicieu Tails per provar "
"d'actualitzar de nou."

#. type: Plain text
msgid ""
"If the problem persists, [[report an error|support]] and include in your "
"report the debugging information that appears in the error message."
msgstr ""
"Si el problema persisteix, [[informeu d'un error|support]] i inclogueu al "
"vostre informe la informació de depuració que apareix al missatge d'error."
