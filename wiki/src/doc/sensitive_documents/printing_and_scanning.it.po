# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-09-04 09:56+0200\n"
"PO-Revision-Date: 2021-03-15 00:06+0000\n"
"Last-Translator: _ignifugo <ignifugo@insicuri.net>\n"
"Language-Team: ita <transitails@inventati.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Printing and scanning\"]]\n"
msgstr "[[!meta title=\"Stampa e scansione\"]]\n"

#. type: Title =
#, no-wrap
msgid "Printing"
msgstr "Stampa"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "To configure a printer or manage your printing jobs choose\n"
#| "<span class=\"menuchoice\">\n"
#| "  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#| "  <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
#| "  <span class=\"guimenuitem\">Settings</span></span>&nbsp;▸\n"
#| "  <span class=\"bold\">Printers</span></span>.\n"
msgid ""
"To configure a printer or manage your printing jobs choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Settings</span></span>&nbsp;▸\n"
"  <span class=\"bold\">Printers</span></span>.\n"
msgstr ""
"Per configurare una stampante o gestire i tuoi lavori di stampa seleziona\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applicazioni</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Strumenti di Sistema</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Impostazioni</span></span>&nbsp;▸\n"
"  <span class=\"bold\">Stampanti</span></span>.\n"

#. type: Plain text
msgid ""
"To check the compatibility of your printer with Linux and Tails, consult the "
"[OpenPrinting database](https://www.openprinting.org/printers) of the Linux "
"Foundation."
msgstr ""
"Per verificare la compatibilità della tua stampante con Linux e Tails, "
"consulta il [database OpenPrinting](https://www.openprinting.org/printers) "
"della Linux Foundation."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<div class=\"tip\">\n"
#| "<p>To reuse the configuration of your printers across different working\n"
#| "sessions, turn on the [[Printers|doc/first_steps/persistence/configure#printers]]\n"
#| "feature of the Persistent Storage.</p>\n"
#| "</div>\n"
msgid ""
"<div class=\"tip\">\n"
"<p>To reuse the configuration of your printers across different working\n"
"sessions, turn on the [[Printers|persistent_storage/configure#printers]]\n"
"feature of the Persistent Storage.</p>\n"
"</div>\n"
msgstr ""
"<div class=\"tip\">\n"
"<p>Per riutilizzare la configurazione delle tue stampanti in sessioni di lavoro differenti, attiva la funzionalità [[Stampanti|doc/first_steps/persistence/configure#printers]] nel Volume Persistente.</p>\n"
"</div>\n"

#. type: Title =
#, no-wrap
msgid "Scanning"
msgstr "Scansione"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Tails includes <span class=\"application\">[Simple\n"
#| "Scan](https://launchpad.net/simple-scan)</span>, a tool to scan both\n"
#| "documents and photos.\n"
msgid ""
"Tails includes the [*Document Scanner*](https://gitlab.gnome.org/GNOME/"
"simple-scan)  utility to scan documents and photos."
msgstr ""
"Tails include <span class=\"application\">[Simple\n"
"Scan](https://launchpad.net/simple-scan)</span>, uno strumento per "
"scansionare sia documenti sia foto.\n"

#. type: Plain text
msgid ""
"To start *Document Scanner*, choose **Applications**&nbsp;▸ "
"**Utilities**&nbsp;▸ **Document Scanner**."
msgstr ""

#, fuzzy, no-wrap
#~| msgid ""
#~| "To start <span class=\"application\">Simple Scan</span> choose\n"
#~| "<span class=\"menuchoice\">\n"
#~| "  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~| "  <span class=\"guisubmenu\">Graphics</span>&nbsp;▸\n"
#~| "  <span class=\"guimenuitem\">Simple Scan</span></span>.\n"
#~ msgid ""
#~ "To start <span class=\"application\">Simple Scan</span> choose\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~ "  <span class=\"guisubmenu\">Graphics</span>&nbsp;▸\n"
#~ "  <span class=\"guimenuitem\">Simple Scan</span></span>.\n"
#~ msgstr ""
#~ "Per avviare <span class=\"application\">Simple Scan</span> seleziona\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Applicazioni</span>&nbsp;▸\n"
#~ "  <span class=\"guisubmenu\">Grafica</span>&nbsp;▸\n"
#~ "  <span class=\"guimenuitem\">Simple Scan</span></span>.\n"
