# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2022-12-22 12:06+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/"
"office_suite/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Office suite\"]]\n"
msgstr "[[!meta title=\"بستهٔ آفیس\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"Tails includes <span class=\"application\">[LibreOffice](https://www.libreoffice.org/)</span>,\n"
"a powerful office suite which embeds several tools:\n"
msgstr "تیلز شامل <span class=\"application\">[لیبره‌آفیس](https://fa.libreoffice.org/)</span>، بستهٔ نرم‌افزاری نیرومندی با ابزارهای متعدد است:\n"

#. type: Plain text
msgid "- *Writer*, a word processor"
msgstr ""

#. type: Plain text
#, fuzzy
msgid "- *Calc*, a spreasheet application"
msgstr "**<span class=\"application\">کالک</span>**، یک ابزار صفحه‌ گسترده"

#. type: Plain text
#, fuzzy
msgid "- *Impress*, a presentation engine"
msgstr "**<span class=\"application\">ایمپرس</span>**، یک موتور ارائه"

#. type: Bullet: '- '
#, fuzzy
msgid "*Draw*, a drawing and flowcharting application"
msgstr ""
"**<span class=\"application\">درا</span>**، ابزاری برای کشیدن و رسم فلوچارت‌ها"

#. type: Plain text
#, fuzzy, no-wrap
msgid ""
"To start <span class=\"application\">LibreOffice</span> choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Office</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">LibreOffice</span></span>.\n"
msgstr ""
"برای راه‌اندازی <span class=\"application\">لیبره‌آفیس</span> این مسیر را بروید:\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">ابزارها</span>&nbsp;◀\n"
"  <span class=\"guisubmenu\">آفیس</span>&nbsp;◀\n"
"  <span class=\"guimenuitem\">لیبره‌آفیس</span></span>.\n"

#. type: Plain text
#, no-wrap
msgid ""
"The <span class=\"application\">LibreOffice</span> website provides complete [user\n"
"guides](https://wiki.documentfoundation.org/Documentation/Publications) for each\n"
"of these tools, translated into several languages.\n"
msgstr ""
"تارنمای <span class=\"application\">لیبره‌آفیس</span> [راهنمای \n"
"کاربر](https://wiki.documentfoundation.org/Documentation/Publications) کاملی برای\n"
"هر کدام از این ابزارها دارد که به زبان‌های مختلفی ترجمه شده‌اند.\n"

#. type: Plain text
#, no-wrap
msgid ""
"To use *LibreOffice* in your language, you can\n"
"install the <code>libreoffice-l10n-<span class=\"command-placeholder\">lang</span></code>\n"
"package using the [[Additional Software|persistent_storage/additional_software]]\n"
"feature. Replace <span class=\"command-placeholder\">lang</span> with the code\n"
"for your language. For example, `vi` for Vietnamese.\n"
msgstr ""

#. type: Plain text
msgid ""
"Tails already includes language packages for Arabic, German, Spanish, Farsi, "
"French, Hindi, Bahasa Indonesia, Italian, Portuguese, Russian, Turkish, and "
"Simplified Chinese."
msgstr ""

#. type: Plain text
msgid ""
"[List of available *LibreOffice* language packages](https://packages.debian."
"org/search?keywords=libreoffice-l10n&searchon=names&suite=stable&section=all)"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents/persistence\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/sensitive_documents/persistence.fa\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents/metadata.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/sensitive_documents/metadata.inline.fa\" raw=\"yes\" sort=\"age\"]]\n"

#~ msgid "**<span class=\"application\">Writer</span>**, a word processor"
#~ msgstr "**<span class=\"application\">رایتر</span>**، یک واژه‌پرداز"

#~ msgid "**<span class=\"application\">Math</span>**, for editing mathematics"
#~ msgstr ""
#~ "**<span class=\"application\">مث</span>**، ابزاری برای ویرایش مسائل ریاضی"
