[[!meta title="Recovering the Persistent Storage of a broken Tails"]]

<!-- XXX: These instructions should be kept in sync with backup.mdwn -->

If your Tails becomes broken, for example, if it no longer starts, follow
these instructions to try to recover data from your Persistent Storage.

<div class="tip">

<p>We recommend you always keep a current backup of your Persistent Storage. To learn
how to create a backup of your Persistent Storage, see our [[instructions on
how to back up your Persistent Storage to another Tails USB
stick|backup]].</p>

</div>

1. Install Tails on a new USB stick onto which you want to recover your Persistent
   Storage. Make sure that this new USB stick is at least
   as large as the USB stick that has your broken Tails on it.

1. Start on your new Tails and [[create a Persistent
   Storage|persistent_storage/create]].

1. Close the Persistent Storage settings after creation, when the list of
   features is displayed.

   The process described below overrides the configuration of the Persistent
   Storage. So, it does not matter which features you turn on after creating
   the Persistent Storage.

1. Restart on your new Tails, set up an [[administration
   password|doc/first_steps/welcome_screen/administration_password]], and
   unlock the Persistent Storage.

   <div class="caution">

   <p>Make sure that all applications are closed before continuing with these
   instructions. Otherwise, the Persistent Storage might not be properly
   recovered from your broken Tails.</p>

   </div>

1. Choose **Applications**&nbsp;▸ **Accessories**&nbsp;▸ **Files** to open the
   *Files* browser.

1. Plug in the USB stick that has your broken Tails on it.

   If the Persistent Storage of your broken Tails is recognized,
   a new encrypted volume appears in the sidebar of the
   *Files* browser. Click on it and enter the
   passphrase of your broken Tails to unlock the volume.

   The Persistent Storage of your broken Tails shows up as
   **TailsData**.

   <div class="bug">

   <p>If no encrypted volume appears, then you might not be able to
   recover the Persistent Storage of your broken Tails.</p>

   <p>Try to [[check the file system of the Persistent Storage|check]] on your
   broken Tails.</p>

   </div>

1. Choose
   <span class="menuchoice">
     <span class="guimenu">Applications</span>&nbsp;▸
     <span class="guisubmenu">System Tools</span>&nbsp;▸
     <span class="guimenuitem">Root Terminal</span>
   </span>
   to open a terminal with administration rights.

1. Execute the following command to recover the Persistent Storage of your
   broken Tails to your new Tails:

   <p class="command-root">rsync -PaSHAXv --del /media/amnesia/TailsData/ /live/persistence/TailsData_unlocked</p>

1. When the command finishes, it displays a summary of the data that was copied. For example:

       sent 32.32M bytes  received 1.69K bytes  21.55M bytes/sec
       total size is 32.30M  speedup is 1.00

   You can now eject the <span class="guilabel">TailsData</span> volume in the
   <span class="application">Files</span> browser and unplug the USB stick that
   has your broken Tails on it.

You can also explore the content of the Persistent Storage on your broken Tails
from the *Files* browser. To do so, execute the following command from the
*Root Terminal*:

   <p class="command-root">nautilus</p>
